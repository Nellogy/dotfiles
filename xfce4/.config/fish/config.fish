if status is-interactive
    # Commands to run in interactive sessions can go here
	set -e SSH_ASKPASS
end

starship init fish | source
