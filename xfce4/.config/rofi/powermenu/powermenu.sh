#!/usr/bin/env bash

## Author  : Aditya Shakya
## Mail    : adi1090x@gmail.com
## Github  : @adi1090x
## Twitter : @adi1090x

# Available Styles
# >> Created and tested on : rofi 1.6.0-1
#
# column_circle     column_square     column_rounded     column_alt
# card_circle     card_square     card_rounded     card_alt
# dock_circle     dock_square     dock_rounded     dock_alt
# drop_circle     drop_square     drop_rounded     drop_alt
# full_circle     full_square     full_rounded     full_alt
# row_circle      row_square      row_rounded      row_alt
dir="$HOME/.config/rofi/powermenu"
rofi_command="rofi -theme $dir/powermenu"

# Options
shutdown="\tShutdown"
shutdown2="	Shutdown"
reboot="\tReboot"
reboot2="	Reboot"
lock="\tLock screen"
lock2="	Lock screen"
suspend="\tSuspend"
suspend2="	Suspend"
logout="﫼\tLogout"
logout2="﫼	Logout"

# Confirmation
confirm_exit() {
	echo -e "Yes\nNo" | rofi -theme $dir/confirm.rasi -dmenu
}

# Variable passed to rofi
options="$shutdown\n$reboot\n$lock\n$suspend\n$logout"

chosen="$(echo -e "$options" | $rofi_command -dmenu)"
case $chosen in
    $shutdown2)
		ans=$(confirm_exit &)
		if [[ $ans == "Yes" ]]; then
			systemctl poweroff
		elif [[ $ans == "No" ]]; then
			exit 0
        fi
        ;;
    $reboot2)
		ans=$(confirm_exit &)
		if [[ $ans == "Yes" ]]; then
			systemctl reboot
		elif [[ $ans == "No" ]]; then
			exit 0
        fi
        ;;
    $lock2)
		ans=$(confirm_exit &)
		if [[ $ans == "Yes" ]]; then
			xflock4
		elif [[ $ans == "No" ]]; then
			exit 0
		fi
        ;;
    $suspend2)
		ans=$(confirm_exit &)
		if [[$ans == "Yes" ]]; then
			mpc -q pause
			amixer set Master mute
			systemctl suspend
		elif [[ $ans == "No" ]]; then
			exit 0
        fi
        ;;
    $logout2)
		ans=$(confirm_exit &)
		if [[ $ans == "Yes" ]]; then
			xfce4-session-logout --logout
		elif [[ $ans == "No" ]]; then
			exit 0
        fi
        ;;
esac