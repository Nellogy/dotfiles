#!/bin/bash
# script to copy all the custom files from its real location into the project folder

# polybar
cp -R -u ~/.config/polybar/ ~/dotfiles/xfce4/.config

# tint2
cp -R -u ~/.config/tint2/ ~/dotfiles/xfce4/.config

# rofi
cp -R -u ~/.config/rofi/ ~/dotfiles/xfce4/.config

# theme (dots bar)
cp -R -u /home/nellogy/.themes/customDots ~/dotfiles/xfce4/.themes/customDots

# fish
# set as default shell
# chsh -s $(which fish)

# configuration command
# fish_config
# fish configuration file
cp -R -u ~/.config/fish ~/dotfiles/xfce4/.config

# starship for terminal
# curl -sS https://starship.rs/install.sh | sh
# startship configuration file
cp -u ~/.config/starship.toml ~/dotfiles/xfce4/.config

# wallpapers
cp -R -u ~/.wallpapers ~/dotfiles/xfce4/.wallpapers

# fonts
cp -R -u ~/.fonts ~/dotfiles/xfce4/.fonts